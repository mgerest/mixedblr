# mixedBLR

MATLAB codes to perform the numerical experiments reported in the article
"Mixed Precision Low Rank Approximation and Their Application to Block Low 
Rank Matrix Factorization " by Patrick Amestoy, Olivier Boiteau, Alfredo 
Buttari, Matthieu Gerest, Fabienne Jézéquel, Jean-Yves L'Excellent and 
Théo Mary.

## Included MATLAB files
* **BLR_facto_mixed.m**: Performs the LU factorization of a dense unsymmetric matrix, using FCSU variant with mixed precision BLR.
* **chop.m**: Used to emulate low precisions (inspired from the function **chop.m** from the [Matrix Computations Toolbox](http://www.ma.man.ac.uk/~higham/mctoolbox/).
* **QRCP_mixed.m**: Computes a QR factorization with column pivoting in mixed precision.

## Included Matrices
The P64 matrix (Poisson) is included as an example in the folder matrix_dir/. To get access to the other matrices please contact the authors.

## Requirements
* The codes have been developed and tested with MATLAB 2020b.

## Example
Example of command to launch a factorization:
b_err = BLRfacto_mixed( 'P64', 128, 1e-10,{'fp64','fp32','bf16'}, 'output_file.txt' );
