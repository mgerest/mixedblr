%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BLRfacto: this function performs the BLR LU factorization of a dense
% unsymmetric matrix, using FCSU variant
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs:
% matrix:  name of the matrix file
% cs:      cluster size
% lrth_in: low-rank threshold used for compression
% precisions_in: a cell array containing 1 + numbers of bits in the
%   mantissa, or containing the name of the formats (example: 'double',
%   'single', fp16', bfloat16')
% outfile: output file where my_fprintf redirects  (if '', no redirection)
% exponents_in (optional): array containing the maximal exponents of each 
%   precision format if the name of the format was not already given.
% BLRfacto_mixed("perf009d", 64, 1E-7, {'fp32' , 'bf16'}, '');
% BLRfacto_mixed("P64", 128, 1E-10, {24, 8}, '', [127, 127] );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Description of the structure of a FR/LR block B structure
%   B.IsLR: 1 if LR, 0 if FR
%   B.X, B.Y: if the block is FR B.X and B.Y are empty;
%       if the block is LR the block is equal to B.X*B.Y (note the absence of 
%       transpose on B.Y!)
%       B.X and B.Y are in fact block matrices (cell arrays), composed of
%       blocks with several precisions.
%   B.B ('B' for 'block'): if FR B.B is the full block else empty
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [b_err, sto, f_err, err_lu, n, A, Afull] = BLRfacto(matrix,cs,lrth_in,precisions_in,outfile, exponents_in)
global exponents_max;
  if(exist('exponents_in', 'var'))
    exponents_max=exponents_in;
  else
    exponents_max=1023*ones(length(precisions_in), 1);
  end
  % Initialization
  [A, n, nb, Afull, nrmA] = init(matrix,cs,lrth_in,precisions_in,outfile);
  global lrth;
  lrth = lrth*nrmA;
  global facto_qr;
  facto_qr=3; %3: Low-rank kernel: QR factorization
  global flops;
  global precisions;
  lp=length(precisions);
  global costs;
  flops=struct();
  flops.LUdiag = 0;
  flops.trsmLR = zeros(lp, 1);
  flops.trsmFR = 0;
  flops.compress = zeros(lp,1);
  flops.updFRFR = 0;
  flops.updLR = zeros(lp, 1);
  flops.precision=zeros(lp,1);
  % Factorization
  A = BLRfacto_main(A,nb);
  % Error check
  [b_err, f_err, err_lu] = errcheck(Afull,A,nb);
  % Print stats
  stoFR = n^2;
  flopsFR = 2*n^3/3;
  [sto.diag, sto.lr] = storageBLR(A,nb);
  sto.total = sto.diag;
  sto.mixed = costs(1) * sto.diag;
  flops.total=0;
  flops.mixed=0;
  flops.precision(1) = flops.LUdiag + flops.trsmFR + flops.updFRFR;
  for k=1:lp
    flops.precision(k) = flops.precision(k) + flops.compress(k) + flops.trsmLR(k) + flops.updLR(k);
    flops.total = flops.total + flops.precision(k);
    flops.mixed = flops.mixed + costs(k) * flops.precision(k);
    sto.total = sto.total + sto.lr(k);
    sto.mixed = sto.mixed + costs(k) * sto.lr(k);
  end
  my_fprintf({'===========================================\n'});
  my_fprintf({'Backward error      = %.2e \n',b_err});
  my_fprintf({'===========================================\n'});
  my_fprintf({'STORAGE RESULTS: \n'});
  my_fprintf({'BLR storage entries           = %.2e (%4.1f%% of FR)\n',sto.total,sto.total/stoFR*100});
  my_fprintf({'    - FR blocks (precision 1) = %.2e (%4.1f%%)\n',sto.diag, sto.diag/sto.total*100});
  for k=1:lp
    my_fprintf({'    - LR blocks (precision %d) = %.2e (%4.1f%%)\n',k,sto.lr(k), sto.lr(k)/sto.total*100});
  end
  my_fprintf({'Projected storage        = %.1f%% of double precision BLR\n',sto.mixed/sto.total*100});
  my_fprintf({'===========================================\n'});
  my_fprintf({'FLOPS RESULTS: \n'});
  my_fprintf({'BLR flops                        = %.2e (%4.1f%% of FR)\n',flops.total, flops.total/flopsFR*100});
  my_fprintf({'    - LU on diag blocks (precision 1) = %.2e (%4.1f%%)\n',flops.LUdiag, flops.LUdiag/flops.total*100});
  for k=1:lp
    my_fprintf({'    - Compress          (precision %d) = %.2e (%4.1f%%)\n',k,flops.compress(k), flops.compress(k)/flops.total*100});
  end
  my_fprintf({'    - TRSM on FR blocks (precision 1) = %.2e (%4.1f%%)\n',flops.trsmFR, flops.trsmFR/flops.total*100});
  for k=1:lp
    my_fprintf({'    - TRSM on LR blocks (precision %d) = %.2e (%4.1f%%)\n',k,flops.trsmLR(k), flops.trsmLR(k)/flops.total*100});
  end
  my_fprintf({'    - Updates FR-FR     (precision 1) = %.2e (%4.1f%%)\n',flops.updFRFR, flops.updFRFR/flops.total*100});
  for k=1:lp
    my_fprintf({'    - Updates LR  (precision %d) = %.2e (%4.1f%%)\n',k,flops.updLR(k), flops.updLR(k)/flops.total*100});
  end  
  my_fprintf({'    ---------------------------------------\n'});
  for k=1:lp
    my_fprintf({'    => Total precision %d          = %.2e (%4.1f%%)\n',k,flops.precision(k), flops.precision(k)/flops.total*100});
  end
  my_fprintf({'Projected time = %.1f%% of double precision BLR (%4.1f%% of FR)\n',flops.mixed/flops.total*100,flops.mixed/flopsFR*100});
  my_fprintf({'===========================================\n\n'});
  % Close output file
  global fout;
  if ~strcmp(outfile,'')
    fclose(fout);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                            FACTORIZATION                               %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function S = BLRfacto_main(S, nb)
% main routine computing the BLR LU factorization
% right-looking FCSU variant is used
% S is an (nb,nb)-cell
% S{i,j} is the (i,j)-th block
  for k=1:nb
    % Factor (F)
    S{k,k}.B = mylu(S{k,k}.B);
    % Compress (C)
    for i=k+1:nb
      s = size(S{i,k}.B);
      S{i,k} = compress(S{i,k}.B,prod(s)/sum(s),'L');
      s = size(S{k,i}.B);
      S{k,i} = compress(S{k,i}.B,prod(s)/sum(s),'U');
    end
    % Solve (S)
    for i=k+1:nb
      S{i,k} = trsm(S{k,k}.B, S{i,k}, 'L');
      S{k,i} = trsm(S{k,k}.B, S{k,i}, 'U');
    end
    % Update (U)
    for i=k+1:nb
      for j=k+1:nb
        % S{i,j} = S{i,j} - S{i,k}*S{k,j}
        % (S{i,j} is FR on output)
        if i>j
          S{i,j} = lrupdate(S{i,j}, S{i,k}, S{k,j}, 'L');
        else
          S{i,j} = lrupdate(S{i,j}, S{i,k}, S{k,j}, 'U');
        end
      end
    end
  end
end

%renvoie la grille des indices des précisions à utiliser dans les blocs du
%produit YaT*Yb
function u=grille(precisions, lrth)
   global grille;
   %1: grille max(i,j) (défaut)
   %2: grille optimisée
   
   if(grille==2)
       %grille optimisée
       p=length(precisions);
       precisions2=[precisions; 1];
       u=zeros(p);
       u(1, 1:p)=1:p;
       u(1:p, 1)=1:p;
       for i=2:p
           for j=2:p
               u(i,j)=find(2.^-precisions2<=2^-precisions(i)* 2^-precisions(j)/lrth, 1, 'last');
           end
       end
   %ou bien, désactiver la gestion avancée des précisions dans le produit interne:
   else %grille==1 (par défaut)
        u=[[1,2,3];
           [2,2,3];
           [3,3,3]];
   end
end

function P=matmulLRLR(A, B, LorU)
    global precisions;
    global lrth;
    precisions_YX=grille(precisions, lrth);
    n=size(A.X{1}, 1);
    m=size(B.Y{1}, 2);
    P.IsLR = 1;
    %M = [[matmul(A.Y{1},B.X{1},'double'),  double(matmul(A.Y{1},B.X{2},'single'))];
    %     [double(matmul(A.Y{2},B.X{1},'single')), double(matmul(A.Y{2},B.X{2},'single'))]];
    lp=length(precisions);
    M=zeros(my_size(A.Y, 1), my_size(B.X, 2));
    n_tmp=0;
    for i=1:lp
        delta_n=size(A.Y{i}, 1);
        if(delta_n>0)
            n_tmp_new = n_tmp + delta_n;
            m_tmp=0;
            for j=1:lp
                delta_m=size(B.X{j}, 2);
                if(delta_m>0)
                    m_tmp_new = m_tmp + delta_m;
                    %M(n_tmp+1:n_tmp_new,m_tmp+1:m_tmp_new)=my_cast(matmul(A.Y{i},B.X{j},max(i,j)), 1);
                    M(n_tmp+1:n_tmp_new,m_tmp+1:m_tmp_new)=my_cast(matmul(A.Y{i},B.X{j},precisions_YX(i,j)), 1);
                    m_tmp=m_tmp_new;
                end
            end
            n_tmp=n_tmp_new;
        end
    end
    
    if min(size(M))==0
      for i=1:lp
        P.X{i}= my_zeros(n,0,i);
        P.Y{i}= my_zeros(0,m,i);
      end
      return
    end
    M = compress(M,min(size(M)),LorU); % toujours compresser M
    %M = compress(M,min(size(M))-1,LorU);
    if M.IsLR
         %P.X = A.X * M.X:
        for i=1:lp
          if (size(M.X{i},2)>0)
            rold = 1;
            rnew = 0;
            for j=1:lp
              rnew = rnew + size(A.X{j},2);
              W{j} = M.X{i}(rold:rnew,:);
              rold = rnew+1;
            end
            % decompress avec une précision maximale de precisions(i)
            P.X{i} = decompress(A.X, W, i);
          else
            P.X{i}= my_zeros(n,0,i);
          end
        end

        %P.Y = M.Y * B.Y:
        for i = 1:lp
          if (size(M.Y{i},1)>0)
            rold = 1;
            rnew = 0;
            for j=1:lp
              rnew = rnew + size(B.Y{j},1);
              W{j} = M.Y{i}(:,rold:rnew);
              rold = rnew+1;
            end
              P.Y{i} = decompress(W, B.Y, i);
          else
            P.Y{i}= my_zeros(0,m,i);
          end
        end
    else
    % M is FR
      error('M FR')
    end
end

function P=matmulFRLR(A, B)
global precisions;
lp=length(precisions);
    assert(B.IsLR && ~A.IsLR);
     P=B;
     %P.IsLR = 1;
     %P.Y = B.Y;
     %P.X = cell(1,lp);  
    for j=1:lp
         P.X{j} = matmul(A.B, B.X{j}, j); 
    end
end

function P=matmulLRFR(A, B)
    global precisions;
    lp=length(precisions);
    assert(A.IsLR && ~B.IsLR)
    P=A;
    %P.IsLR = 1;
    %P.X = A.X;
    %P.Y=cell(lp, 1);
    for i=1:lp
        P.Y{i} = matmul(A.Y{i}, B.B, i);
    end
end

function Z = decompress(X, Y, prec)
% computes X*Y
% with
% X= {[X{1} X{2}]};
% Y= {[Y{1}];
%     [Y{2}]}
global precisions;
lp=length(precisions);
if ~exist('prec','var')
    prec=1;
end
%X*Y=W1+(W2+(W3+...)):
  %boucle décroissante:
  debut=1;
  for k=lp:-1:1
    if(debut && size(X{k},2)>0)
        Z=matmul(X{k}, Y{k}, max(prec,k));
        debut=0;
    elseif(~debut && size(X{k},2)>0)
        Wk=matmul(X{k}, Y{k}, max(prec,k));
        Z = matadd(Z, Wk, max(prec,k));
    else
        %matrice vide * matrice vide = matrice nulle (ignorée)
    end
  end
  if(debut)
      %cas rare d'une matrice nulle compressée
      Z=my_zeros(size(X{1}, 1), size(Y{1}, 2), 1);
      error("des calculs en trop sur des matrices nulles: à éviter");
  end
end

function C = matmul(A, B, precision, FRFR)
% compute C = A*B
% with precision(C)=precision (default: 'double')
% useful to isolate this in a function for flop counting
  global t_double;
  global t_single;
  global precisions;
  global exponents_max;
  if ~exist('precision','var')
  	precision=1;
  end
  if ~exist('FRFR','var')
    FRFR = 0;
  end  
  [n, m] = size(A); p = size(B,2);

% C=my_cast(A, precision)*my_cast(B,precision);
%Ou bien: une autre méthode qui simule également tous les calculs intermédiaires dans la bonne précision:
   if(precision>length(precisions))
       C=my_zeros(n, p, length(precisions)); %Calculs non effectués
       return;
   elseif(precisions(precision)==t_double)
       C=double(A)*double(B);
   elseif(precisions(precision)==t_single)
       C=single(A)*single(B);
   else
     C = zeros(n,p);
     A=chop(A, precisions(precision), exponents_max(precision));
     B=chop(B, precisions(precision), exponents_max(precision));
     for k=1:m
       C = C + chop(single(A(:,k))*single(B(k,:)), precisions(precision), exponents_max(precision));
     end
   end

  global flops;
  [n, m] = size(C);
  p=size(A,2);
  if p==0
    return
  end
  newflops=2*n*m*p - m*n;
  if FRFR
    flops.updFRFR = flops.updFRFR + newflops;
  else
  	flops.updLR(precision) = flops.updLR(precision) + newflops;
  end
end


function B = my_cast(A, prec)
    global t_double;
    global t_single;
    global precisions;
    global exponents_max;
    if(precisions(prec)==t_double)
        B=double(A);
    elseif(precisions(prec)==t_single)
        B=single(A);
    else
        B=chop(single(A), precisions(prec), exponents_max(prec));
    end
end


function s=my_size(A,dim)
% compute the size of a block matrix
    s=0;
    for k=1:size(A, dim)
        s = s+size(A{k},dim);
    end
end


function C = matadd(A, B, precision, FRFR)
% compute C = A+B
% with precision(C)=precision (default: 'double')
% just as for matmul, useful for flop counting
  if  ~exist('precision','var')
  	precision=1;
  end
  if ~exist('FRFR','var')
    FRFR = 0;
  end
%   if(~any(A(:)) || ~any(B(:)) ) % A==0 || B==0
%   test pour savoir si on fait des additions en trop
%   end
  	C = my_cast(A,precision) +my_cast(B,precision);
 newflops = prod(size(C));
 global flops;
 global precisions;
  if FRFR
    flops.updFRFR = flops.updFRFR + newflops;
  else
  	flops.updLR(precision) = flops.updLR(precision) + newflops;
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Z = trsm(T, B, LorU)
% computes triangular solve Z*triu(T) = B (L case) or tril(T)*Z = B (U case)
% if LorU = U
%    we need to compute Z = T\B (= T^{-1}B)
%    if B is LR: Z.X = T\B.X (and Z.Y = B.Y)
%    T\B.X=[T\B.X1, T\B.X2, ...]
%    if B is FR: Z.B = T\B.B
% if LorU = L
%    we need to compute Z = B/T (= BT^{-1})
%    if B is FR
%       then so is Z and Z.B = B.B/T
%    if B is LR
%       B/T = B.X*B.Y*T^{-1} = B.X * (B.Y/T)
%       Z.X = B.X, Z.Y = B.Y/T=[B.Y1/T; B.Y2/T; ...]
  global flops;
  Z = B;
  L = tril(T,-1)+eye(length(T));
  U = triu(T);
  if LorU=='U'
      if B.IsLR
        %Z.X = L\B.X:
        Z.X=cell(1, length(B.X));
        for j=1:length(B.X)
            ZXj=my_cast(L, j)\B.X{j};
            Z.X{j}=my_cast(ZXj,j);
        end
        
        %TODO: voir si trsm peut utiliser les précisions pour avoir moins de flops, pour toute précision
        for k=1:length(flops.trsmLR)
            flops.trsmLR(k) = flops.trsmLR(k) + length(L)*(length(L)-1)*size(B.X{k},2);
        end
      else
        Z.B = L\B.B;
        flops.trsmFR = flops.trsmFR + length(L)*(length(L)-1)*size(B.B,2);
      end

  elseif B.IsLR
    %Z.Y = B.Y/U:
    Z.Y=cell(length(B.Y), 1);
    for j=1:length(B.Y)
        ZYj=B.Y{j}/my_cast(U,j);
        Z.Y{j}=my_cast(ZYj,j);
    end
    
    for k=1:length(flops.trsmLR)
    	flops.trsmLR(k) = flops.trsmLR(k) + prod(size(U))*size(B.Y{k},1);
    end
  else
    Z.B = B.B/U;
    flops.trsmFR = flops.trsmFR + prod(size(U))*size(B.B,1);
  end

end

function Z = lrupdate(C, A, B, LorU)
% computes the update Z = C-A*B (the result is FR)
% in FCSU variant C is never LR
% if both A and B are LR:
%    1/ compute M = A.Y*B.X
%    2/ compress M = M.X*M.Y
%    3/ compute Z = C - (A.X*M.X) * (M.Y*B.Y) (FR)
% if A is LR and B is FR
%    Z = C - A.X*(A.Y*B)
% if A is FR and B is LR
%    Z = C - (A*B.X)*B.Y
% if both A and B are FR:
%    Z = C - A*B
global precisions;
  Z = C;
  if A.IsLR && B.IsLR
    P=matmulLRLR(A, B, LorU);
    if(my_size(P.X, 2)>0)
        Z.B = matadd(C.B, -1*decompress(P.X,P.Y), 1);
    end
  elseif A.IsLR
    P = matmulLRFR(A, B);
    if(my_size(P.X, 2)>0)
        Z.B = matadd(C.B, -1*decompress(P.X,P.Y), 1);
    end
  elseif B.IsLR
    P = matmulFRLR(A, B);
    if(my_size(P.X, 2)>0)
        Z.B = matadd(C.B, -1*decompress(P.X,P.Y), 1);
    end
  else
    P.B = matmul(A.B, B.B, 1, 1); P.IsLR = 0;
    Z.B = matadd(C.B, -1*P.B, 1, 1);
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function B = compress(A, kmax, LorU)
% Compute a LR approximation B.X*B.Y \approx A
% A is given as a FR matrix
% If rank(A) <= kmax
%    B is declared LR and represented as B.X*B.Y
% Else
%    B is declared FR and stored exactly in B.B
% Compression is based on truncated SVD with absolute threshold
% The singular values are multiplied to the left if LorU = L and to the right
% if LorU = U. This is done to insure outer orthonormality.
% After the compression, some of the singular values and the associated
% vectors are converted into single precision.
  global precisions;
  global exponents_max;
  lp=length(precisions);
  global lrth;
  %lrth_corrected=lrth/(2*lp-1);
  
  global facto_qr;
  %0: svd
  %1: qr fait maison
  %2: qr mixte fait maison (utilise correction_seuil_1 et correction_seuil_2)
  %3: qr de matlab (défaut)
  if(isempty(facto_qr))
      facto_qr=3;
  end
  
  %global condition_lr;
  % 1: critère LR mixte (défaut): r1+0.5*r2+0.25*r3 <= kmax/2
  % 2: ancien critère LR: r <= kmax/2
  %if(isempty(condition_lr))
  %    condition_lr=1;
  %end
  
  global costs;
  ranks=zeros(lp+1,1); %[0; r2; r3; rk]
  if(facto_qr>0)
      %qr à la place de svd
      if LorU=='U'
          if(facto_qr==1)
            [Q,R,P,rk] = QRCP_double(A',lrth);
          elseif(facto_qr==2)
            [Q,R,P,rk] = QRCP_mixed(A',lrth, precisions, exponents_max);
          else
            [Q, R, P]=qr(A');
          end
          Svec = abs(my_diag(R));
          rk = sum(Svec>lrth);
          X=P * R(1:rk, :)';
          Y=Q(:,1:rk)';
      else
          if(facto_qr==1)
            [Q,R,P,rk] = QRCP_double(A,lrth);
          elseif(facto_qr==2)
            [Q,R,P,rk] = QRCP_mixed(A,lrth, precisions, exponents_max);
          else
            [Q, R, P]=qr(A);
          end
          Svec = abs(my_diag(R));
          rk = sum(Svec>lrth);
          X=Q(:, 1:rk); 
          Y=R(1:rk, :) * P';
      end
  else %facto_qr==0: svd
      [U,Smat,V] = svd(A);
      Svec = abs(my_diag(Smat));
      rk = sum(Svec>lrth);
      if LorU=='U'
        X = U(:,1:rk)*Smat(1:rk,1:rk);  Y = V(:,1:rk)';
      else
        X = U(:,1:rk); Y = Smat(1:rk,1:rk)*V(:,1:rk)';
	  end
  end
      
  % commented line below corresponds to relative threshold
  %rk = sum(abs(Svec)>lrth*abs(Svec(1)));
  
  for i=2:lp
  	ranks(i)=sum(Svec>lrth/(2^-precisions(i)));
  end
  ranks(lp+1)=rk;
  
  r_corrected=0;
  for k=1:lp
      r_corrected = r_corrected + costs(k)/costs(1)*(ranks(k+1)-ranks(k));
  end
  %if(condition_lr==1)
  admissible = (r_corrected<=kmax);
  %else
  %    admissible = (rk<=kmax);
  %end
  
  if (admissible)
    B.IsLR = 1;
    B.X=cell(1, lp);
    B.Y=cell(lp, 1);
    for i=1:lp
          X_i=X(:, ranks(i)+1:ranks(i+1));
          B.X{i}=my_cast(X_i, i);
          Y_i=Y(ranks(i)+1:ranks(i+1), :);
          B.Y{i}=my_cast(Y_i, i);
    end
  else
    B.IsLR = 0;
    B.B = A;
  end
  global flops;
  [m, n] = size(A);
  %TODO: pas besoin de compter autant de flops en trop si le bloc n'est pas admissible
 
  if(facto_qr==1 || facto_qr==3) %QR double
      r = min(rk,kmax);
      % HR cost
      flops.compress(1) = flops.compress(1) + 4*m*n*r - (2*m+n)*r*r + r*r*r/3;
      if B.IsLR % Build Q cost
      flops.compress(1) = flops.compress(1) + 2*m*r*r - r*r*r/3;
      end
  elseif(facto_qr==2) %QR mixte
      
      %calcul des précisions utilisées dans la facto qr
      global correction_seuil_1; 
      global correction_seuil_2;
      correction_seuils=[1,correction_seuil_1, correction_seuil_2, 1];
      ranks_QR=zeros(lp+1,1); %[0; r2; r3; rk]
      for i=2:lp
        ranks_QR(i)=sum(Svec>correction_seuils(i)*lrth/(2^-precisions(i)));
      end
      ranks_QR(lp+1)=rk;
      
      for i=1:lp
        % HR cost
        ri = ranks_QR(i+1) - ranks_QR(i);
        flops.compress(i) = flops.compress(i) + 2*ri*ri*(m-ri/3) + (n-ranks_QR(i+1))*ri*(4*m - ri);
        if B.IsLR % Build Q cost
        flops.compress(i) = flops.compress(i) + 2*m*ri*ri - ri*ri*ri;
        end
      end
  else
        %flops svd...TODO
  end
end

function Svec=my_diag(Smat)
% 'diag' a un comportement different sur une matrice et sur un vecteur
% Dans 'my_diag', on traite les vecteurs comme des matrices
      if min(size(Smat))>1
        Svec = diag(Smat);
      else
        Svec = Smat(1);
      end
end

function Z=my_zeros(n,m,prec)
    global precisions;
    global t_double;
    if precisions(prec)==t_double
        Z=zeros(n,m,'double');
    else
        Z=zeros(n,m,'single');
    end
end

function A = blockmatrix2array(B)
% Convert a block matrix to a full matrix in double precision
    precision='double';
    for i=1:length(B)
        B{i}=cast(B{i}, precision);
    end
    A=cell2mat(B);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function B = mylu(A)
% computes A = LU where LU factors are stored in place in B
% LU without pivoting is used
  % lu with no pivoting lu(A,0) is only available for sparse matrices
  [L,U] = lu(sparse(double(A)),0);
  %[L, U] = lu_nopivot(my_cast(A, 1));
  B = my_cast(tril(full(L),-1) + full(U), 1);
  global flops;
  flops.LUdiag = flops.LUdiag + 2/3*size(A,1)^3;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                             INITIALIZATION                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [A,n,nb,Afull, nrmA] = init(matrix,cs,lrth_in,precisions_in,outfile)
% initialize global variables
  global fout;
  if strcmp(outfile,'')
    fout = 0;
  else
    fout = fopen(outfile,'a');
  end
  global lrth;
  lrth = lrth_in;
  global exponents_max; %pour chaque précision, la valeur de l'exposant maximal
  global precisions;
  global t_double;
  t_double=53;
  global t_single;
  t_single=24;
  global costs; %accélérations attribuées à chaque précision

  precisions=zeros(length(precisions_in), 1);
  costs=zeros(length(precisions_in), 1);
  for k=1:length(precisions_in)
      if ischar(precisions_in{k}) && ismember(precisions_in{k}, {'d','double','fp64'})
          costs(k)=1;
          precisions(k) = 53;
          exponents_max(k) = 1023;
      elseif ischar(precisions_in{k}) && ismember(precisions_in{k}, {'s','single','fp32'})
          costs(k)=1/2;
          precisions(k) = 24;
          exponents_max(k) = 127;
      elseif ischar(precisions_in{k}) && ismember(precisions_in{k}, {'b','bf16','bfloat16'})
          costs(k)=1/4;
          precisions(k) = 8;
          exponents_max(k) = 127;
      elseif ischar(precisions_in{k}) && ismember(precisions_in{k}, {'h','half','fp16'})
          costs(k)=1/4;
          precisions(k) = 11;
          exponents_max(k) = 15;
      elseif ischar(precisions_in{k}) && ismember(precisions_in{k}, {'tf32'})
          costs(k)=19/64;
          precisions(k) = 11;
          exponents_max(k) = 127;
      else
          precisions(k) = precisions_in{k};
          costs(k)=(precisions(k)+log2(exponents_max(k)+1)+1)/64;
      end
  end

% read matrix
  path='./matrix_dir/';
  f = fopen(sprintf('%s/root_%s_cs%d.bin',path,matrix,cs));
  Afull = fread(f,'double');
  fclose(f);
  n = size(Afull,1); n = sqrt(n);
  Afull = reshape(Afull,n,n);
  Afull = my_cast(Afull, 1);
  nrmA = norm(Afull, 'fro');
% read clusters
  f = fopen(sprintf('%s/clusters_%s_cs%d.txt',path,matrix,cs));
  clusters = fscanf(f,'%e');
  fclose(f);
  nb = length(clusters)-1;
  A = cell(nb);
  for i=1:nb
    for j=1:nb
      A{i,j}.B = Afull(clusters(i):clusters(i+1)-1,clusters(j):clusters(j+1)-1);
      A{i,j}.IsLR = 0;
    end
  end
  
%   %supprimer les precisions qui ne doivent pas être utilisées, pour essayer d'obtenir u1<lrth<u2<...
%   I=find(2.^-precisions>lrth); %indices des précisions faibles à garder
%   if(isempty(I)) %toutes les formats utilisés sont trop précis: on prend le + faible
%       precisions=precisions(end);
%       costs=costs(end);
%       exponents_max=exponents_max(end);
%   else
%       i2=I(1);%indice de la 2e précision à garder
%       if(i2>1)
%           I_kept=[(i2-1); I];
%           precisions=precisions(I_kept);
%           costs=costs(I_kept);
%           exponents_max=exponents_max(I_kept);
%       else 
%           %on avait lrth<u1<... donc on garde tous les formats
%       end
%   end
  
  lp=length(precisions);
% print info
  my_fprintf({'===========================================\n'});
  my_fprintf({'Matrix name        = %s\n',matrix});
  my_fprintf({'Matrix size        = %d\n',n});
  my_fprintf({'Number of blocks   = %d\n',nb});
  my_fprintf({'Block size         = %d\n',cs});
  my_fprintf({'Low-rank threshold = %.0e\n',lrth});
  %Print info about precisions
  for k=1:lp
    my_fprintf({'Precision %2d: %d mantissa bits + %2d exponent bits',k,precisions(k),log2(exponents_max(k)+1)+1});
    if(precisions(k)==t_double && exponents_max(k)==1023)
        my_fprintf({' (fp64)\n'});
    elseif(precisions(k)==t_single && exponents_max(k)==127)
        my_fprintf({' (fp32)\n'});
    elseif(precisions(k)==11 && exponents_max(k)==15)
        my_fprintf({' (fp16)\n'});
    elseif(precisions(k)==8 && exponents_max(k)==127)
        my_fprintf({' (bfloat16)\n'});
    elseif(precisions(k)==11 && exponents_max(k)==127)
        my_fprintf({' (tf32)\n'});
    else
        my_fprintf({'\n'});
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                    ERROR CHECKING AND DEBUGGING                        %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [L,U]=reconstruct(S, n)
    LU=zeros(n, n);
    nb=size(S, 1);
    jend=0;
    for j=1:nb
        jbeg = jend+1;
        jend = jbeg + size(S{j,j}.B,1) -1;
        
        iend = 0;
        for i=1:nb
          ibeg = iend+1;
          if S{i,j}.IsLR
            iend = ibeg + size(S{i,j}.X{1},1) -1; 
            LU(ibeg:iend, jbeg:jend)=blockmatrix2array(S{i,j}.X)*(blockmatrix2array(S{i,j}.Y));
          else
            iend = ibeg + size(S{i,j}.B,1) -1;
            LU(ibeg:iend, jbeg:jend)=S{i,j}.B;
          end
        end    
    end
    L = tril(LU,-1)+eye(n);
    U = triu(LU);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x = solve(S,nb,rhs)
  y = fwd_solve(S,nb,rhs);
  x = bwd_solve(S,nb,y);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = fwd_solve(S,nb,rhs)
  y=rhs;
  jend = 0;
  for j=1:nb
    jbeg = jend+1;
    jend = jbeg + size(S{j,j}.B,1) -1;
    Ljj = tril(S{j,j}.B,-1)+eye(size(S{j,j}.B));
    y(jbeg:jend,:) = Ljj\y(jbeg:jend,:);
    iend = jend;
    for i=j+1:nb
      ibeg = iend+1;
      if S{i,j}.IsLR
        iend = ibeg + size(S{i,j}.X{1},1) -1; %TODO: factoriser le code
        y(ibeg:iend,:) = y(ibeg:iend,:) - blockmatrix2array(S{i,j}.X)*(blockmatrix2array(S{i,j}.Y)*y(jbeg:jend,:));
      else
        iend = ibeg + size(S{i,j}.B,1) -1;
        y(ibeg:iend,:) = y(ibeg:iend,:) - S{i,j}.B*y(jbeg:jend,:);
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x = bwd_solve(S,nb,y)
  x=y;
  ibeg = size(y,1)+1;
  for i=nb:-1:1
    iend = ibeg-1;
    ibeg = iend - size(S{i,i}.B,1) +1;
    jend = iend;
    for j=i+1:nb
      jbeg = jend+1;
      if S{i,j}.IsLR
        jend = jbeg + size(S{i,j}.Y{1},2) -1; %TODO: factoriser le code de calcul de la taille
        x(ibeg:iend,:) = x(ibeg:iend,:) - blockmatrix2array(S{i,j}.X)*(blockmatrix2array(S{i,j}.Y)*x(jbeg:jend,:));
      else
        jend = jbeg + size(S{i,j}.B,2) -1;
        x(ibeg:iend,:) = x(ibeg:iend,:) - S{i,j}.B*x(jbeg:jend,:);
      end
    end
    Uii = triu(S{i,i}.B);
    x(ibeg:iend,:) = Uii\x(ibeg:iend,:);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [b_err, f_err, err_lu] = errcheck(Afull, S, nb)
  xtrue = ones(size(Afull,2),1);
  rhs = Afull*xtrue;
  x1 = solve(S,nb,rhs);
  normA = norm(Afull,'fro');
  normx = norm(xtrue,'fro');
  b_err = norm(Afull*x1-rhs)/(normA*normx);
  f_err = norm(x1-xtrue)/norm(xtrue);
  %[L, U]=reconstruct(S, size(Afull, 1));
  err_lu = NaN; %norm(Afull-L*U)/norm(Afull);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%           STATS RELATED FUNCTIONS (STORAGE, RANKS, ETC)                %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function my_fprintf(args)
% printf to either file or terminal
  global fout;
  if fout == 0
    fprintf(args{1:end});
  else
    fprintf(fout,args{1:end});
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [sFR, sLR] = storageBLR(S,nb)
  global precisions;
  lp=length(precisions);
  sLR=zeros(lp,1);
  sFR=0;
  for i=1:nb
  for j=1:nb
    if S{i,j}.IsLR
      for k=1:lp
          sLR(k)=sLR(k)+prod(size(S{i,j}.X{k})) + prod(size(S{i,j}.Y{k}));
      end
    else
      sFR = sFR + prod(size(S{i,j}.B));
    end
  end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                                EOF                                     %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
