function [Q,R,P,rk] = QRCP(A, lrth, precisions, exponents_max);
global correction_seuil_1; %seuil entre double et single, pour le qrcp: correction_seuil_1*norm(A)*eps/us
global correction_seuil_2; %seuil entre single et half, pour le qrcp: correction_seuil_1*norm(A)*eps/us
t_double=53;
t_single=24;
  if(~exist('precisions', 'var'))
      precisions=[53, 24, 8];
      exponents_max=[1024, 127, 127];
  end;
  if(isempty(correction_seuil_1)) %variables globales non initialisées
      correction_seuil_1=1/10;
      correction_seuil_2=1/10;
  end
  lp=length(precisions);

  [m,n] = size(A);
  kmax = min(m,n);
  % column norms
  for j = 1:n
    colnrm(j) = norm(A(:,j));
    colnrm_save(j) = colnrm(j);
  end
  Q = eye(m);
  R = A;
  jpvt = 1:n;
  rk = kmax;
  current_prec = 1; %1, 2 or 3
  for k = 1:kmax
    % find col with max norm
    [maxnrm,ipiv] = max(colnrm(k:end));
    if maxnrm <= lrth
      rk = k-1;
      break
    end
    if lp>=3 && maxnrm <= correction_seuil_2*lrth/2^-precisions(3)
      current_prec = 3;
      t=precisions(3);
      e=exponents_max(3);
    elseif lp >=2 && maxnrm <= correction_seuil_1*lrth/2^-precisions(2)
      current_prec = 2;
      t=precisions(2);
      e=exponents_max(2);
    end
    ipiv = ipiv+k-1;
    % swap columns, norms, and jpvt
    temp = R(:,k);
    R(:,k) = R(:,ipiv);
    R(:,ipiv) = temp;
    temp = jpvt(k);
    jpvt(k) = jpvt(ipiv); 
    jpvt(ipiv) = temp;
    colnrm(ipiv) = colnrm(k);
    colnrm_save(ipiv) = colnrm_save(k);
    % generate HH reflector and update Q and R
    v = R(k:end,k);
    if precisions(current_prec)==t_double
      v(1) = v(1) + mysign(v(1))*norm(v);
      v = v/norm(v);
      Q(:,k:end) = Q(:,k:end) - 2*(Q(:,k:end)*v)*v';
      R(k:end,:) = R(k:end,:) - 2*v*(v'*R(k:end,:));
    elseif precisions(current_prec)==t_single
      v=single(v);
      v(1) = v(1) + mysign(v(1))*norm(v);
      v = v/norm(v);
      Q(:,k:end) = double(single(Q(:,k:end)) - 2*(single(Q(:,k:end))*v)*v');
      R(k:end,:) = double(single(R(k:end,:)) - 2*v*(v'*single(R(k:end,:))));
    else
      v(1) = chop(v(1) + mysign(v(1))*norm_chop(v, t, e), t, e);
      v = chop(v/norm_chop(v, t, e), t, e);
      Q(:,k:end) = chop(Q(:,k:end) - 2*matmul(matmul(Q(:,k:end), v, t, e), v', t, e), t, e);
      R(k:end,:) = chop(R(k:end,:) - 2*matmul(v, matmul(v', R(k:end,:), t, e), t, e), t, e);
    end
    % update col norms as advised in LAWN 176
    if precisions(current_prec)==t_double
      for j = k+1:n
        temp = (abs(R(k,j))/colnrm(j));
        temp = max(0, (1+temp)*(1-temp));
        temp2 = temp*(colnrm(j)/colnrm_save(j))^2;
        if temp2 <= sqrt(eps)
          colnrm(j) = norm(R(k+1:end,j));
          colnrm_save(j) = colnrm(j);
        else
          colnrm(j) = colnrm(j)*sqrt(temp);
        end
      end
    elseif precisions(current_prec)==t_single
      for j = k+1:n
        temp = (abs(single(R(k,j)))/single(colnrm(j)));
        temp = max(0, (1+temp)*(1-temp));
        temp2 = temp*(single(colnrm(j))/single(colnrm_save(j)))^2;
        if temp2 <= sqrt(eps)
          colnrm(j) = double(norm(single(R(k+1:end,j))));
          colnrm_save(j) = colnrm(j);
        else
          colnrm(j) = double(single(colnrm(j))*sqrt(temp));
        end
      end  
    else
      for j = k+1:n
        temp = chop(abs(R(k,j))/colnrm(j), t, e);
        temp = max(0, chop(chop(1+temp,t, e)*chop(1-temp,t, e),t, e));
        temp2 = chop(temp * chop( (chop(colnrm(j)/colnrm_save(j),t, e))^2, t, e), t, e);
        if temp2 <= sqrt(eps)
          colnrm(j) = norm_chop(R(k+1:end,j), t, e);
          colnrm_save(j) = colnrm(j);
        else
          colnrm(j) = chop(colnrm(j)*chop(sqrt(temp),t, e),t, e);
        end
      end
    end
  end
  if rk == kmax && abs(R(kmax,kmax)) <= lrth
    rk = kmax-1;
  end
  for k=1:n
    P(jpvt(k),k)=1;
  end
end

function s = mysign(x)
  if x==0
    s = 1;
  else
    s = sign(x);
  end
end

function C = matmul(A, B, t, e)
  [m,n] = size(A);
  [~,p] = size(B);
  C = zeros(m,p);
  for k = 1:n
    C = chop(C + chop(A(:,k)*B(k,:), t, e), t, e);
  end
end

function s = norm_chop(x, t, e)
  s = 0;
  for i=1:length(x)
    s = chop(s + chop(x(i)^2, t, e), t, e);
  end
  s = chop(sqrt(s), t, e);
end

%function c = chop(x, t)
%  y = abs(x) + (x==0);
%  e = floor(log2(y) + 1);
%  c = pow2(round( pow2(x, t-e) ), e-t);
%end
