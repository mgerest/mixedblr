function c = chop(x, t, emax)%CHOP    Round matrix elements.
%        CHOP(X, t, emax) is the matrix obtained by rounding the elements of X
%        to t significant binary places, with exponents e<=emax.
emin=1-emax;
%  Use the representation:
%  x(i,j) = 2^e(i,j) * .d(1)d(2)...d(s) * sign(x(i,j))
%  On the next line `+(x==0)' avoids passing a zero argument to LOG, which
%  would cause a warning message to be generated.
y = abs(x) + (x==0);
e = floor(log2(y) + 1);
%c = (e >= emin+1) ./ (e<=emax+1) .* pow2(round( pow2(x, t-e) ), e-t);
c = (e >= emin+1) .* pow2(round( pow2(x, t-e) ), e-t);
%c = pow2(round( pow2(x, t-e) ), e-t);
end
